/*
* Deck.java
*
* Implements a standard 52 card playing deck
*
* Joey Idler
*/

import java.util.ArrayList;
import java.util.Collections;

public class Deck {

	private static final int SIZE = 52;	// number of cards in a deck
	private List<Card> cards = new ArrayList<Card>(SIZE); 	// holds all of the cards

	/*
	* Constructor 
	*
	* Builds a 52 card deck with all combinations of suit and rank
	*
	*/
	public Deck() {

		for( Suit suit : Suit.values() )
			for( Rank rank : Rank.values() )
				this.cards.add( new Card(suit, rank) );
	}

	/*
	* Constructor for building shoes
	*
	* numDecks- number of decks in shoe
	*/
	public Deck(final int numDecks) {

		this.cards.ensureCapacity(SIZE * numDecks);

		for(int i = 1; i <= numDecks; i++)
			for( Suit suit : Suit.values() )
				for( Rank rank : Rank.values() )
					this.cards.add( new Card(suit, rank) );
	}

	/*
	* Shuffle the deck
	*
	*/
	public final void shuffle() {

		Collections.shuffle(this.cards);
	}

	/*
	* Draw top card
	*
	*/
	public final Card draw() {

		return this.deck.remove(0);
	}

}
