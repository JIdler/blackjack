/*
* Hand.java
*
* implements a dealer/players card hand
*
* Joey Idler
*/

import java.util.ArrayList;

public class Hand {

	private List<Card> hand = new ArrayList<Card>(2);
	private int score = 0; // score of hand
	private boolean isSoft = false;

	// Constructor
	public Hand(final Card card) {

		this.hand.add(card);
	}

	/*
	* score the hand
	*
	*/
	public final int getScore() {

		return score;
	}

	/*
	* hit
	*
	*/
	public final void add(final Card card) {

		hand.add(card);
	}

	/*
	* split the hand 
	*
	*
	*/
	public final Hand split() {

		Card card = hand.remove(hand.size() -1);

		return new Hand(card);
	}



}
