/*
* Card.java class
*
* Represents a playing card in a deck of cards
*
* Joey Idler
*/

public class Card {

	private final Suit suit; 
	private final Rank rank;

	public Card(Suit suit, Rank rank) {

		this.suit = suit;
		this.rank = rank;
	}

	/*
	* 
	* returns this cards cononical value
	*/
	public final value() {

		return this.rank.getValue();
	}

	/*
	* Overriden toString method 
	*
	*/
	@Override
	public final String toString() {

		return "[" + this.rank + " of " + this.suit + "]";
	}

}
